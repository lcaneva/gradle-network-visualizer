# Gradle hierarchical network visualizer

A tool to visualize the dependency graph of a multiproject Gradle build as a hierarchical network, in order to be able to better identify the emerging relationships existing among different subgroups of the network.

Usage
---
The project consists in a reactive Angular application that uses D3.js to plot a network. 
Therefore, to view it locally one needs only to run the following commands:

```
cd <repository>
npm install
npm start
```

The resulting page will be visible on `localhost:4200`. 

For sake of practicity, a static page showing the output plot is published using a Gitlab CI/CD pipeline and can be accessed at URL https://lcaneva.gitlab.io/gradle-network-visualizer/.

Example
---
Here it is an example of the plot:

![network](/src/assets/network_example.png)

The names of the projects are clearly fictitious.

Licensing notes
---
The project is an elaboration of the ObservableHQ's notebook [Hierarchical Edge Bundling](https://observablehq.com/@d3/hierarchical-edge-bundling) released under ISC License by Mike Bostock.

```
Copyright 2018–2020 Observable, Inc.
Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
```
