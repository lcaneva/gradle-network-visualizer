import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as d3 from "d3";
import * as raw_network_data from '../../assets/allowedDependencies.json';

interface RawData {
  [key: string]: string[]
}
interface CrossDependency {
  name: string;
  imports?: string[];
  children?: any[];
}

type Node = d3.HierarchyNode<CrossDependency>

const colorin = "#00f";
const colorout = "#f00";
const colornone = "#ccc";

const GROUPS =
  [
    "other",
    "zea-",
    "spe-",
    "cal-",
    "cdg-",
    "mag-",
    "acc-",
    "mes-",
    "pro-",
    "wfk-",
    "doc-",
    "coc-",
    "cap-"
  ];

const DEPENDENCIES_TYPES =
  [
    "-db-meta",
    "-db",
    "-helpers",
    "-ext",
    "-logic",
    "-logic-impl",
    "-ui",
    "-ext-components",
    "mc-persistence",
    "mc-ui",
    "-mail"
  ];

@Component({
  selector: 'app-network',
  template: `
    <div id='chart'>
    </div>
  `,
  styleUrls: ['./network.component.scss'],
  // Disattivo l'incapsulamento css per poter gestire le classi del grafico 
  encapsulation: ViewEncapsulation.None,
})
export class HierarchicalNetworkComponent implements OnInit {

  private svg!: d3.Selection<SVGSVGElement, unknown, HTMLElement, any>;
  private root!: d3.HierarchyPointNode<any>;

  constructor() {
  }

  ngOnInit(): void {

    const data: CrossDependency[] = this.readJson();

    const preprocessedData = this.preprocessData(data, "exclude", ["-db", "-ext"]);
    const dataRoot = this.postprocess(this.processHierarchicalData(preprocessedData));

    const containerNode = (d3.select("div#chart").node() as any).getBoundingClientRect();
    const totalWidth = containerNode.width;
    const totalHeight = totalWidth;
    const radius = totalWidth / 2;

    const treeLayoutFn: d3.ClusterLayout<unknown> = d3.cluster()
      .size([2 * Math.PI, 0.6 * radius]);

    const lineFn = d3.lineRadial()
      .curve(d3.curveBundle.beta(0.85))
      .radius((d: any) => d.y)
      .angle((d: any) => d.x);

    const rootNodeD3: Node = d3.hierarchy(dataRoot)
      .sort((a, b) => d3.ascending(a.height, b.height) || d3.ascending(a.data.name, b.data.name));

    this.root = treeLayoutFn(this.bilink(rootNodeD3));

    this.svg = d3.select("div#chart")
      .append("div")
      .classed("svg-container", true)
      .append("svg")
      .attr("preserveAspectRatio", "xMinYMin meet")
      .attr("viewBox", `0 0 ${totalWidth} ${totalHeight}`)
      .classed("svg-content-responsive", true);


    const linkBetweenProjects = this.svg.append("g")
      .attr('transform', `translate(${totalWidth / 2}, ${totalHeight / 2})`)
      .attr("stroke", colornone)
      .attr("fill", "none")
      .selectAll("path")
      .data(this.root.leaves().flatMap((leaf: any) => leaf.outgoing))
      .join("path")
      .style("mix-blend-mode", "multiply")
      .attr("d", ([i, o]: any) => lineFn(i.path(o)))
      .each(function (d: any) {
        d.path = this;
      });

    const projectNode = this.svg.append("g")
      .attr('transform', `translate(${totalWidth / 2}, ${totalHeight / 2})`)
      .selectAll("g")
      .data(this.root.leaves())
      .join("g")
      .attr("transform", (d: any) => `rotate(${d.x * 180 / Math.PI - 90}) translate(${d.y},0)`)
      .append("text")
      .attr("dy", "0.31em")
      .attr("x", (d: any) => d.x < Math.PI ? 6 : -6)
      .attr("text-anchor", (d: any) => d.x < Math.PI ? "start" : "end")
      .attr("transform", (d: any) => d.x >= Math.PI ? "rotate(180)" : null)
      .text((d: any) => d.data.name.replace("standard", "std"))
      .each(function (d: any) {
        d.text = this;
      })
      .on("mouseover", function (e: any, d: any) {
        linkBetweenProjects.style("mix-blend-mode", null);
        d3.select(this).attr("font-weight", "bold");
        d3.selectAll(d.incoming.map((d: any) => d.path)).attr("stroke", colorin).raise();
        d3.selectAll(d.incoming.map(([d]: [any]) => d.text)).attr("fill", colorin).attr("font-weight", "bold");
        d3.selectAll(d.outgoing.map((d: any) => d.path)).attr("stroke", colorout).raise();
        d3.selectAll(d.outgoing.map(([, d]: any) => d.text)).attr("fill", colorout).attr("font-weight", "bold");
      })
      .on("mouseout", function (e: any, d: any) {
        linkBetweenProjects.style("mix-blend-mode", "multiply");
        d3.select(this).attr("font-weight", null);
        d3.selectAll(d.incoming.map((d: any) => d.path)).attr("stroke", null);
        d3.selectAll(d.incoming.map(([d]: any[]) => d.text)).attr("fill", null).attr("font-weight", null);
        d3.selectAll(d.outgoing.map((d: any) => d.path)).attr("stroke", null);
        d3.selectAll(d.outgoing.map(([, d]: any[]) => d.text)).attr("fill", null).attr("font-weight", null);
      })
      .call(text => text.append("title")
        .text((d: any) => `${this.id(d)}
          ${d.outgoing.length} outgoing
          ${d.incoming.length} incoming`)
      );

  }

  private readJson() {
    const rawData: RawData = (raw_network_data as any) as RawData;
    const data: CrossDependency[] = [];
    for (const key in rawData) {
      if (key !== "default") {
        data.push({ "name": key, "imports": rawData[key] });
      }
    }
    return data;
  }

  private processHierarchicalData(data: CrossDependency[], delimiter = "."): CrossDependency {
    let root: CrossDependency = { name: "root", children: [] };
    const map = new Map;
    let groupNodes = GROUPS.map(g => ({ name: g, children: [] }));
    groupNodes.forEach((g: CrossDependency) => {
      map.set(g.name, g);
      root.children?.push(g);
    });

    data.forEach(function find(dataElement) {
      const { name, imports } = dataElement;
      if (map.has(name)) return map.get(name);
      map.set(name, dataElement);
      let group = GROUPS.find(g => name.startsWith(g)) ?? "other_group";
      let type = DEPENDENCIES_TYPES.find(d => name.includes(d)) ?? "other_dep";
      let groupNode = map.get(group);
      let typeNode = groupNode.children.find((c: any) => c.name === type);
      if (!typeNode) {
        typeNode = ({ name: type, children: [] });
        groupNode.children.push(typeNode);
      }
      typeNode.children.push(dataElement);
      imports?.forEach(i => find({ name: i, imports: [] }));
      return dataElement;
    });
    return root;
  }

  // Funzione di preprocessing dei dati del grafo per includere solo alcune categorie di progetto o per escluderne altre
  private preprocessData(data: CrossDependency[], filterType: ("exclude"|"include"),  types: string[]) {
    // Se il filtro è esclusivo, il nome del progetto non deve contenere nessuna sottostringa passata da fuori
    // Se inclusivo, deve contenerne almeno una
    const filterFn = filterType == "exclude" 
      ? (s: string) => types.every(type => !s.includes(type))
      : (s: string) => types.some(type => s.includes(type))
    return data.filter(r => filterFn(r.name))
      .map(r => ({ ...r, imports: r.imports?.filter(filterFn) }))
      .filter(r => r.imports?.length);
  }

  private postprocess(root: CrossDependency) {
    // Toglie i nodi che non vengono raggiunti da nessun vertice
    // Utile nel caso di filtri in fase di preprocessing
    root.children = root.children
      ?.filter((c: any) => c.children.length);
      // ?.filter((c:any) => c.outgoing?.length && c.ingoing?.length);
    return root;
  }

  private id(node: Node): string {
    return node.data.name;
  }

  private bilink(root: Node): Node {
    const map = new Map(root.leaves().map((d: any) => [this.id(d), d]));
    for (const d of root.leaves() as any) {
      d.incoming = []
      d.outgoing = d.data.imports ? d.data.imports.map((i: any) => [d, map.get(i)]) : [];
    }
    for (const d of root.leaves() as any)
      for (const o of d.outgoing)
        o[1].incoming.push(o);

    return root;
  }

}
